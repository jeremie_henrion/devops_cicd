import matplotlib.pyplot as plt
from sklearn import datasets

digits = datasets.load_digits()
plt.figure(1, figsize=(3, 3))
plt.imshow(digits.images[10], cmap=plt.cm.gray_r, interpolation='nearest')
plt.show()