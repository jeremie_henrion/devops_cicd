#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
from pymongo import MongoClient 
from datetime import datetime


API_KEY="3699e2c07a830815b6597a2aa84c11e0"


db_url = "mongodb://mongodb:mongodb@localhost/"

# get all company
# url = (f"https://financialmodelingprep.com/api/v3/delisted-companies?page=0&apikey={API_KEY}")
# companies = [e["symbol"] for e in requests.get(url).json()]

# get companies profiles
# profiles = []
# for c in companies[0:10]:
#     p = []
#     while p == []:
#         url = (f"https://financialmodelingprep.com/api/v3/profile/{c}?apikey={API_KEY}")
#         p = requests.get(url).json()
#         profiles.append(requests.get(url).json()[0])


def get_data(company):
    url = (f"https://financialmodelingprep.com/api/v3/profile/{company}?apikey={API_KEY}")
    data_profile = requests.get(url).json()
    print(data_profile)
    url = (f"https://financialmodelingprep.com/api/v3/rating/{company}?apikey={API_KEY}")
    data_rating = requests.get(url).json()[0]
    data = {"profile" : data_profile,
            "rating": data_rating,
            "time": datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
            }

    return data


# insertion en db
def insert_db(data):
    client = MongoClient("mongodb://mongodb:mongodb@localhost/")
    db = client["airflow"]
    col = db["data"]
    new = col.insert_one(data)
    return new


d = get_data("AAPL")
insert_db(d)