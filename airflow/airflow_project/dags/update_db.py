# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

import requests, airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from pymongo import MongoClient
import json
from jsontomongo import JsonToMongoOperator

API_KEY = "3699e2c07a830815b6597a2aa84c11e0"

DB_URL = "mongodb://mongodb:mongodb@mongo:27017/"


def get_profile():
    company = "AAPL"
    url = (f"https://financialmodelingprep.com/api/v3/profile/{company}?apikey={API_KEY}")
    data_profile = requests.get(url).json()[0]
    with open('/tmp/json_profile.json', 'w') as outfile:
        json.dump(data_profile, outfile)

def get_rating():
    company = "AAPL"
    url = (f"https://financialmodelingprep.com/api/v3/rating/{company}?apikey={API_KEY}")
    data_rating = requests.get(url).json()[0]
    with open('/tmp/json_rating.json', 'w') as outfile:
        json.dump(data_rating, outfile)

# # insertion en db
# def insert_db():
#     with open('/tmp/json_data.json', 'r') as outfile:
#         data = json.load(outfile)
    
#     client = MongoClient(DB_URL)
#     db = client["airflow"]
#     col = db["data"]
#     new = col.insert_one(data)
    
#     return new


# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'jeremie',
    'start_date': airflow.utils.dates.days_ago(0), 
    'schedule_interval': timedelta(days=1)
}

dag = DAG(
    'update_db',
    default_args=default_args,
    description='Airflow project',
    schedule_interval=timedelta(days=1),
)

py_get_profile = PythonOperator(
        task_id = 'get_profile',
        python_callable = get_profile,
        dag = dag
    )

py_get_rating = PythonOperator(
        task_id = 'get_rating',
        python_callable = get_rating,
        dag = dag
    )

insert_profile = JsonToMongoOperator(
        file_to_load = "/tmp/json_profile.json", 
        mongoserver = "mongo", 
        mongouser = "mongodb",
        mongopass = "mongodb",
        task_id = 'insert_data_profile',
        dag = dag
    )

insert_rating = JsonToMongoOperator(
        file_to_load = "/tmp/json_rating.json", 
        mongoserver = "mongo", 
        mongouser = "mongodb",
        mongopass = "mongodb",
        task_id = 'insert_data_rating',
        dag = dag
    )

py_get_profile >> insert_profile
py_get_rating >> insert_rating
